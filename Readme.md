## Task

We need to develop a simple blog site on React and Node.
As a data source, we will use a http://jsonplaceholder.typicode.com/posts endpoint

## Requirements

1. We should have an import command to get posts from the data source
2. All should be written in TS
3. Backend server should be written without any frameworks
4. Need to add docker-compose file
5. User can be mocked
6. All requests should use JWT. (Posts should be available only for authenticated users)
7. At the end you should provide a link to the bitbucket repository

## Notes
### Env authorization parameters configured for 'demo' purposes
1. Token valid time set to 30s (1h by default)
2. "Refresh token" valid time set to 15m (14d by default)
3. JWT default secret is 'samplesecret' (algorithm: 'HS256')
4. Mocked user email: 'john@example.com', password: 'john1234'
