import React from 'react';
import { Link } from 'react-router-dom';
import Navigation from './Navigation';

const Header: React.FC = () => {
  return (
    <div className="header">
      <div className="header__bar">
        <div className="logo">
          <Link to="/">
            <h1>Simple blog</h1>
          </Link>
        </div>
        <Navigation />
      </div>
    </div>
  );
};

export default Header;
