import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { actions as authActions } from '../../state/auth';
import useLoginStatus from '../../common/hooks/useLoginStatus';

const AuthButton: React.FC = () => {
  const isLoggedIn = useLoginStatus();
  const dispatch = useDispatch();
  const history = useHistory();

  const handleClickLogout = () => {
    dispatch(authActions.logout());
    history.push('/');
  };

  return isLoggedIn ? (
    <li>
      <button className="link-button" onClick={handleClickLogout}>
        Logout
      </button>
    </li>
  ) : (
    <li>
      <Link to="/login">Login</Link>
    </li>
  );
};

export default AuthButton;
