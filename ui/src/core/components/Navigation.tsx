import React from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import AuthButton from './AuthButton';

const Navigation: React.FC = () => {
  const { pathname } = useLocation();

  return pathname === '/login' ? null : (
    <nav className="header__bar__nav">
      <ul>
        <li>
          <NavLink to="/">Posts</NavLink>
        </li>
        <AuthButton />
      </ul>
    </nav>
  );
};

export default Navigation;
