import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import useLoginStatus from '../../common/hooks/useLoginStatus';

const ProtectedRoute: React.FC<any> = ({ children, ...rest }) => {
  const isLoggedIn = useLoginStatus();

  return <Route {...rest}>{isLoggedIn ? children : <Redirect to="/login" />}</Route>;
};

export default ProtectedRoute;
