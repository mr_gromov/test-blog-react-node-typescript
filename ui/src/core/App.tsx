import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import configureStore from '../store';
import Header from './components/Header';
import ProtectedRoute from './components/ProtectedRoute';
import Login from '../scenes/Login';
import Post from '../scenes/Post';
import Posts from '../scenes/Posts';
import './App.scss';

const { store, persistor } = configureStore();

const App: React.FC = () => (
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <div className="container">
          <Router>
            <Header />

            <Switch>
              <Route path="/login">
                <Login />
              </Route>

              <ProtectedRoute path="/" exact>
                <Posts />
              </ProtectedRoute>

              <ProtectedRoute path="/posts/:postId" exact>
                <Post />
              </ProtectedRoute>

              <Route path="/">
                <Redirect to="/" />
              </Route>
            </Switch>
          </Router>
        </div>
      </PersistGate>
    </Provider>
  </React.StrictMode>
);

export default App;
