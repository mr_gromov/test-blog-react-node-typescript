import { IAuthParams, IToken, ITokens, IPost } from '../types';

interface IAPIResponseFailure {
  status: 'failure';
  error: string;
}

interface IAPIResponseSuccess<T = any> {
  status: 'success';
  data: T;
}

type IAPIResponse<T> = IAPIResponseFailure | IAPIResponseSuccess<T>;

type ITokenRefreshCallback = (data: ITokens) => void;
type IUnauthorizedCallback = () => void;

class API {
  tokens?: ITokens | null;
  onTokensRefresh?: ITokenRefreshCallback | null;
  onUnauthorized?: IUnauthorizedCallback | null;

  constructor(params: {
    tokens?: ITokens | null;
    onTokensRefresh?: ITokenRefreshCallback | null;
    onUnauthorized?: IUnauthorizedCallback | null;
  }) {
    this.tokens = params.tokens;
    this.onTokensRefresh = params.onTokensRefresh;
    this.onUnauthorized = params.onUnauthorized;
  }

  // Add auto tokens refresh
  postJson = async <T>(
    url: string,
    body?: {},
    token?: string
  ): Promise<IAPIResponse<T>> => {
    const options = { method: 'POST' };
    const headers = new Headers({ 'Content-Type': 'application/json' });

    // Optional authorization
    if (token) headers.append('Authorization', `Bearer ${token}`);
    Object.assign(options, { headers });

    // Optional body params
    if (body) {
      Object.assign(options, { body: JSON.stringify(body) });
    }

    return fetch(url, options).then((res) => res.json());
  };

  private refreshTokens = async (data: ITokens) => {
    const response = await this.postJson<ITokens>('/api/refresh', data);

    if (response.status === 'failure') {
      throw new Error(response.error);
    }

    return response.data;
  };

  fetchJson = async <T>(
    url: string,
    queryParams?: {},
    tokens?: ITokens | IToken,
    onTokensRefresh?: ITokenRefreshCallback
  ): Promise<T> => {
    const authTokens = tokens || this.tokens;

    // Optional authorization
    const headers = new Headers();
    if (authTokens?.token) headers.append('Authorization', `Bearer ${authTokens.token}`);

    // Optional params
    let query = '';
    if (queryParams) query = '?' + new URLSearchParams(queryParams).toString();

    const fetchResponse = await fetch(`${url}${query}`, { method: 'GET', headers });
    const response = (await fetchResponse.json()) as IAPIResponse<T>;

    if (response.status === 'success') {
      return response.data;
    } else {
      if (
        response.error === 'Token error: jwt expired' &&
        authTokens?.token &&
        (authTokens as ITokens).refreshToken
      ) {
        // Expired token, refresh and try again
        const newTokens = await this.refreshTokens(authTokens as ITokens);

        // Run callbacks
        this.onTokensRefresh && this.onTokensRefresh(newTokens);
        onTokensRefresh && onTokensRefresh(newTokens);

        // Another try without ability to refresh
        return this.fetchJson(url, queryParams, { token: newTokens.token });
      }

      if (fetchResponse.status === 401) {
        this.onUnauthorized && this.onUnauthorized();

        throw new Error('Unauthorized');
      }

      throw new Error(response.error);
    }
  };

  auth = async ({ email, password }: IAuthParams) => {
    const response = await this.postJson<ITokens>('/api/auth', { email, password });

    if (response.status === 'failure') {
      throw new Error(response.error);
    }

    return response.data;
  };

  fetchPosts = (tokens?: IToken | ITokens, onTokensRefresh?: ITokenRefreshCallback) =>
    this.fetchJson<IPost[]>(`/api/posts`, undefined, tokens, onTokensRefresh);

  fetchPost = (
    id: number | string,
    tokens?: ITokens | { token: string },
    onTokensRefresh?: ITokenRefreshCallback
  ) => this.fetchJson<IPost>(`/api/posts/${id}`, undefined, tokens, onTokensRefresh);
}

export default API;
