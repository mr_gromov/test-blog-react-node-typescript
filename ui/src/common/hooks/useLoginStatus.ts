import { useSelector } from 'react-redux';
import { IRootState } from '../../store';

export default function useLoginStatus() {
  const isLoggedIn = useSelector<IRootState, boolean>((state) => !!state.auth.data);
  return isLoggedIn;
}
