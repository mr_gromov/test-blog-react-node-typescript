import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { actions as postActions } from '../../state/post';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../../store';
import { IPost } from '../../types';

const Post: React.FC = () => {
  const post = useSelector<IRootState, IPost | null>(
    (state) => state.post.data
  );
  const dispatch = useDispatch();
  const { postId } = useParams();

  useEffect(() => {
    dispatch(postActions.fetchPost(postId));
  }, [postId, dispatch]);

  if (!post) {
    return null;
  }

  const { title, body } = post;

  return (
    <article className="post post--standalone">
      <section className="post__header">
        <h3>{title}</h3>
      </section>
      <section className="post__content">{body}</section>
    </article>
  );
};

export default Post;
