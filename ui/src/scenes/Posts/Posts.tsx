import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PostItem from './components/PostItem';
import { actions as postsActions } from '../../state/posts';
import { IPost } from '../../types';
import { IRootState } from '../../store';

const Posts: React.FC = () => {
  const dispatch = useDispatch();
  const posts = useSelector<IRootState, IPost[]>((store) => store.posts.data);

  useEffect(() => {
    dispatch(postsActions.fetchPosts());
  }, [dispatch]);

  return (
    <main className="posts">
      {!posts.length && <h3 style={{ textAlign: 'center' }}>Loading...</h3>}
      {!!posts.length && posts.map((post) => <PostItem {...post} key={post.id} />)}
    </main>
  );
};

export default Posts;
