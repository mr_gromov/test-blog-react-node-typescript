import React from "react";
import { Link } from "react-router-dom";
import { IPost } from "../../../types";

const PostItem: React.FC<IPost | null> = (post) => {
  if (!post) {
    return null;
  }

  const { title, body, id } = post;

  return (
    <article className="post">
      <Link to={`/posts/${id}`}>
        <section className="post__header">
          <h3>{title}</h3>
        </section>
        <section className="post__content">{body}</section>
      </Link>
    </article>
  );
};

export default PostItem;
