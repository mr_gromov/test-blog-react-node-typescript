import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { actions as authActions } from '../../state/auth';
import useLoginStatus from '../../common/hooks/useLoginStatus';

// REFACTOR:
// 1. Extract fields into separate component
// 2. Per field validation (touched fields)

const Login: React.FC = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [messages, setMessages] = useState<string[]>([]);

  const dispatch = useDispatch();

  const isLoggedIn = useLoginStatus();
  const history = useHistory();

  useEffect(() => {
    isLoggedIn && history.push('/');
  }, [history, isLoggedIn]);

  function validateForm() {
    const msg = [];

    if (!email.includes('@') || !email.includes('.')) {
      msg.push('Email should contain "@" and "."');
    }

    if (!(password.length > 6)) {
      msg.push('Password should be min 6 char length');
    }

    setMessages(msg);
  }

  function handleSubmit(e: React.FormEvent) {
    e.preventDefault();
    validateForm();
    if (!messages.length) {
      dispatch(authActions.login({ email, password }));
    }
  }

  return (
    <div className="login-flex-container">
      <div className="login-main-content">
        <form className="login-form form" onSubmit={handleSubmit}>
          <div className="form__group">
            <input
              autoFocus
              placeholder="email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>

          <div className="form__group">
            <input
              placeholder="password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>

          <div className="form__group">
            {messages.length > 0 &&
              messages.map((msg, i) => (
                <p key={i} className="form__error">
                  {msg}
                </p>
              ))}
          </div>

          <div className="form__group">
            <button type="submit" onClick={handleSubmit}>
              Login
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
