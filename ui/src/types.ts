export interface IPost {
  id: number;
  userId: number;
  title: string;
  body: string;
}

// auth endpoint:
export interface IAuthParams {
  email: string;
  password: string;
}

export interface IToken {
  token: string;
}

export interface IRefreshToken {
  refreshToken: string;
}

export type ITokens = IToken & IRefreshToken;
