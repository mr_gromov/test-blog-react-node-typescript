import { IPost } from "../../types";
import { IPostsActions } from "./actions";

export interface IPostsState {
  data: IPost[];
  error: Error | null;
  pending: boolean;
}

const initialState: IPostsState = {
  data: [],
  error: null,
  pending: false,
};

export default function (state = initialState, action: IPostsActions): IPostsState {
  switch (action.type) {
    case "FETCH_POSTS_PENDING":
      return {
        ...state,
        pending: true,
      };

    case "FETCH_POSTS_RESOLVED":
      return {
        data: action.payload,
        error: null,
        pending: false,
      };

    case "FETCH_POSTS_REJECTED":
      return {
        ...state,
        pending: false,
        error: action.payload,
      };

    default:
      return state;
  }
}
