import { IPost } from "../../types";

export function fetchPosts() {
  return {
    type: "FETCH_POSTS",
  } as const;
}

export function fetchPostsPending() {
  return {
    type: "FETCH_POSTS_PENDING",
  } as const;
}

export function fetchPostsResolved(posts: IPost[]) {
  return {
    type: "FETCH_POSTS_RESOLVED",
    payload: posts,
  } as const;
}

export function fetchPostsRejected(error: Error) {
  return {
    type: "FETCH_POSTS_REJECTED",
    payload: error,
  } as const;
}

export function fetchPostsCancel(message?: string) {
  return {
    type: "FETCH_POSTS_CANCEL",
    payload: message,
  } as const;
}

export type IPostsActions =
  | ReturnType<typeof fetchPosts>
  | ReturnType<typeof fetchPostsPending>
  | ReturnType<typeof fetchPostsResolved>
  | ReturnType<typeof fetchPostsRejected>
  | ReturnType<typeof fetchPostsCancel>;
