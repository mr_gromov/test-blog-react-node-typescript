import { IPost } from "../../types";

export function fetchPost(payload: string | number) {
  return {
    type: "FETCH_POST",
    payload,
  } as const;
}

export function fetchPostPending() {
  return {
    type: "FETCH_POST_PENDING",
  } as const;
}

export function fetchPostResolved(post: IPost) {
  return {
    type: "FETCH_POST_RESOLVED",
    payload: post,
  } as const;
}

export function fetchPostRejected(error: Error) {
  return {
    type: "FETCH_POST_REJECTED",
    payload: error,
  } as const;
}

export function fetchPostAborted(message?: string) {
  return {
    type: "FETCH_POST_ABORTED",
    payload: message,
  } as const;
}

export type IPostActions =
  | ReturnType<typeof fetchPost>
  | ReturnType<typeof fetchPostPending>
  | ReturnType<typeof fetchPostResolved>
  | ReturnType<typeof fetchPostRejected>
  | ReturnType<typeof fetchPostAborted>;
