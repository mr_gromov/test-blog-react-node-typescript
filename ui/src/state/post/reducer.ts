import { IPost } from "../../types";
import { IPostActions } from "./actions";

export interface IPostState {
  error: Error | null;
  data: IPost | null;
  pending: boolean;
}

const initialState: IPostState = {
  data: null,
  error: null,
  pending: false,
};

export default function (state = initialState, action: IPostActions): IPostState {
  switch (action.type) {
    case "FETCH_POST_PENDING":
      return {
        ...initialState,
        pending: true,
      };

    case "FETCH_POST_RESOLVED":
      return {
        ...state,
        pending: false,
        data: action.payload,
      };

    case "FETCH_POST_REJECTED":
      return {
        ...state,
        pending: false,
        error: action.payload,
      };

    default:
      return state;
  }
}
