import { ITokens } from "../../types";
import { IAuthActions } from "./actions";

export interface IAuthState {
  pending: boolean;
  data: ITokens | null;
  error: Error | null;
}

const initialState: IAuthState = {
  pending: false,
  data: null,
  error: null,
};

export default function (state = initialState, action: IAuthActions): IAuthState {
  switch (action.type) {
    case "LOGIN_PENDING":
      return {
        ...state,
        error: null,
        pending: true,
      };

    case "LOGIN_RESOLVED":
      return {
        data: action.payload,
        error: null,
        pending: false,
      };

    case "LOGIN_REJECTED":
      return {
        ...state,
        pending: false,
        error: action.payload,
      };

    case "LOGOUT":
      return {
        ...initialState
      };

    default:
      return state;
  }
}
