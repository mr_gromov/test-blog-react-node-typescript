import { IAuthParams, ITokens } from "../../types";

export function login(payload: IAuthParams) {
  return {
    type: "LOGIN",
    payload,
  } as const;
}

export function logout() {
  return {
    type: "LOGOUT",
  } as const;
}

export function loginPending() {
  return {
    type: "LOGIN_PENDING",
  } as const;
}

export function loginResolved(payload: ITokens) {
  return {
    type: "LOGIN_RESOLVED",
    payload,
  } as const;
}

export function loginRejected(payload: Error) {
  return {
    type: "LOGIN_REJECTED",
    payload,
  } as const;
}

export type IAuthActions =
  | ReturnType<typeof login>
  | ReturnType<typeof logout>
  | ReturnType<typeof loginPending>
  | ReturnType<typeof loginResolved>
  | ReturnType<typeof loginRejected>;
