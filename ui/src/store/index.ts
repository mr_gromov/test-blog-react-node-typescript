/* eslint-disable no-underscore-dangle */
import { applyMiddleware, createStore, combineReducers, compose } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import * as storage from "localforage";
import apiMiddleware from "./middlewares/apiMiddleware";
import post from "../state/post";
import posts from "../state/posts";
import auth from "../state/auth";

const rootReducer = combineReducers({
  auth,
  post,
  posts,
});

export type IRootState = ReturnType<typeof rootReducer>;

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["auth"],
};

const persistedReducers = persistReducer(persistConfig, rootReducer);

type WindowExt = Window &
  typeof globalThis & {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  };

const composeEnhancers =
  (window as WindowExt).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  persistedReducers,
  composeEnhancers(applyMiddleware(apiMiddleware))
);

const persistor = persistStore(store);

export default () => {
  return { store, persistor };
};
