import { Middleware } from 'redux';
import API from '../../utils/api';
import { IRootState } from '..';
import { actions as authActions } from '../../state/auth';
import { actions as postActions } from '../../state/post';
import { actions as postsActions } from '../../state/posts';

type IAction = authActions.IAuthActions | postActions.IPostActions | postsActions.IPostsActions;

const apiMiddleware: Middleware<{}, IRootState> = ({ dispatch, getState }) => (next) => (
  action: IAction
) => {
  next(action);

  const api = new API({
    tokens: getState().auth.data,
    onTokensRefresh: (data) => dispatch(authActions.loginResolved(data)),
    onUnauthorized: () => dispatch(authActions.logout()),
  });

  switch (action.type) {
    case 'LOGIN':
      dispatch(authActions.loginPending());

      api
        .auth(action.payload)
        .then((data) => dispatch(authActions.loginResolved(data)))
        .catch((err) => dispatch(authActions.loginRejected(err)));

      break;

    case 'FETCH_POST':
      dispatch(postActions.fetchPostPending());

      api
        .fetchPost(action.payload)
        .then((data) => dispatch(postActions.fetchPostResolved(data)))
        .catch((err) => dispatch(postActions.fetchPostRejected(err)));

      break;

    case 'FETCH_POSTS':
      dispatch(postsActions.fetchPostsPending());

      api
        .fetchPosts()
        .then((data) => dispatch(postsActions.fetchPostsResolved(data)))
        .catch((err) => dispatch(postsActions.fetchPostsRejected(err)));

      break;

    default:
      break;
  }
};

export default apiMiddleware;
