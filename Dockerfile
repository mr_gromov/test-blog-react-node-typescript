FROM node:12-alpine as blog-ui

WORKDIR /usr/src/blog-ui
COPY ./ui .
RUN npm i --silent && npm run build && rm -rf node_modules


FROM node:12-alpine as blog-api

WORKDIR /usr/src/blog-api
COPY ./api .
COPY --from=blog-ui /usr/src/blog-ui/build /var/www/blog
RUN npm i --silent && npm run build
