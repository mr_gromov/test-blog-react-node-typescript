import fs from 'fs';
import path from 'path';
import { UsersStore, PostsStore } from '../types';

// TODO:
// 1. Provide simple api instead of objects

export const users: UsersStore = JSON.parse(
  fs.readFileSync(path.join(__dirname, '..', 'data', 'users.json'), 'utf-8')
);

export const posts: PostsStore = JSON.parse(
  fs.readFileSync(path.join(__dirname, '..', 'data', 'posts.json'), 'utf-8')
);
