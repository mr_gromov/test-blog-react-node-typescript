/* eslint-disable no-console */
import fs from 'fs';
import path from 'path';
import { createServer } from 'http';
import bodyParser from 'body-parser';
import App from './lib/App';
import { protectPath, staticFiles } from './middlewares';
import { auth, refresh } from './routes';
import { posts } from './db';
import { jsonRes } from './utilities';

// Todo:
// 1. Extract constants to config
// 2. Write App '.catch()' method

const { NODE_ENV, STATIC_CONTENT } = process.env;
const PORT = process.env.PORT || 8080;

const app = new App();

app.use(bodyParser.json());

if (NODE_ENV !== 'production') {
  // Log requests
  app.use(({ url, pathname, searchParams, body, headers }, _res, next) => {
    console.log({ url, pathname, searchParams, body });
    console.log(headers);
    next();
  });
}

app.post('/api/auth', auth);
app.post('/api/refresh', refresh);

app.get('/api/about', (_req, res) => {
  res.setHeader('Content-Type', 'text/plain; charset=UTF-8');
  res.end('Simple API written with Node and TypeScript');
});

app.get('/api/posts', protectPath);
app.get('/api/posts', (_req, res) => {
  res.setHeader('Content-Type', 'application/json; charset=UTF-8');
  res.end(
    jsonRes({
      status: 'success',
      data: Object.values(posts),
    })
  );
});

app.get('/api/posts/:id', protectPath);
app.get('/api/posts/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json; charset=UTF-8');

  if (
    Object.prototype.hasOwnProperty.call(posts, req.params.id) &&
    !isNaN(Number(req.params.id))
  ) {
    res.end(
      jsonRes({
        status: 'success',
        data: posts[Number(req.params.id)],
      })
    );
  } else {
    res.statusCode = 404;
    res.end(
      jsonRes({
        status: 'failure',
        error: 'Post not found',
      })
    );
  }
});

if (STATIC_CONTENT) {
  app.use(staticFiles(STATIC_CONTENT));

  // Workaround for React BrowserRouter
  app.get('*', (req, res, next) => {
    if (req.headers.accept?.startsWith('text/html')) {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      fs.createReadStream(path.resolve(STATIC_CONTENT, 'index.html')).pipe(res);
    } else {
      next();
    }
  });
}

app.use((_req, res) => {
  res.statusCode = 404;
  res.end('NOT FOUND');
});

const server = createServer(app.listener);

server.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
  if (STATIC_CONTENT) {
    console.log(`Serving static content from ${STATIC_CONTENT}`);
  }
});
