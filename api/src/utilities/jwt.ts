import jwt from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';
import { TokenPayload } from '../types';

// REFACTOR: extract to config
const JWT_SECRET = process.env.JWT_SECRET || 'samplesecret';
const TOKEN_VALID_TIME = process.env.TOKEN_VALID_TIME || 3600; // 1h
const REFRESH_TOKEN_VALID_TIME = process.env.REFRESH_TOKEN_VALID_TIME || 60 * 60 * 24 * 14; // 14d

// REFACTOR:
// 1. Create persist revoked tokens storage
// 2. Automatically remove expired tokens
interface IRevokedTokensStorage {
  [token: string]: {
    token: string;
    exp: number;
  };
}

const revoked: IRevokedTokensStorage = {};

interface RefreshMethodParams {
  refreshToken: string;
  token: string;
}

interface GeneratePairParams {
  [key: string]: any;
}

export default class JWT {
  static sign = (tokenPayload: GeneratePairParams): Promise<string> => {
    const type = tokenPayload.type || 'regular';

    let validTimeSeconds = TOKEN_VALID_TIME;
    if (type === 'refresh') validTimeSeconds = REFRESH_TOKEN_VALID_TIME;

    const exp = Math.floor(Date.now() / 1000 + Number(validTimeSeconds));

    return new Promise((resolve, reject) => {
      jwt.sign(
        { ...tokenPayload, type, exp },
        JWT_SECRET,
        { algorithm: 'HS256' },
        (error, token) => {
          if (error) {
            reject(error);
          }

          resolve(token);
        }
      );
    });
  };

  static decode = <T>(token: string, options: jwt.VerifyOptions = {}) => {
    return new Promise<T | undefined>((resolve, reject) => {
      if (revoked[token]) {
        reject('Token is revoked');
      }

      jwt.verify(
        token,
        JWT_SECRET,
        { ...options, algorithms: ['HS256'] },
        (err, decoded) => {
          if (err) {
            reject(err);
          }

          if (decoded === undefined) resolve(undefined);

          resolve(decoded as T | undefined);
        }
      );
    });
  };

  /**
   * Decode expired tokens
   */
  static decodeExpired = async <T>(token: string) => {
    return JWT.decode<T>(token, { ignoreExpiration: true });
  };

  static generatePair = async (payload: GeneratePairParams) => {
    const id = uuidv4();

    const token = await JWT.sign({ ...payload, id, type: 'regular' });
    const refreshToken = await JWT.sign({ tokenId: id, type: 'refresh' });

    return { token, refreshToken };
  };

  static refresh = async ({
    refreshToken,
    token,
  }: RefreshMethodParams): Promise<RefreshMethodParams> => {
    const refreshTokenPayload = await JWT.decode<TokenPayload>(refreshToken);

    // Accept expired main token
    const tokenPayload = await JWT.decodeExpired<TokenPayload>(token);

    // check tokens
    if (!refreshTokenPayload || refreshTokenPayload.type !== 'refresh') {
      throw new Error('Invalid refresh token');
    }

    if (!tokenPayload || tokenPayload.type !== 'regular') {
      throw new Error('Invalid token');
    }

    if (tokenPayload.id !== refreshTokenPayload.tokenId) {
      throw new Error('Tokens do not match each other');
    }

    // Revoke previous tokens
    revoked[refreshToken] = {
      token: refreshToken,
      exp: refreshTokenPayload.exp,
    };

    // Revoke only not expired regular tokens
    if (Number(tokenPayload.exp) * 1000 > new Date().getTime()) {
      console.log(Number(tokenPayload.exp) * 1000, new Date().getTime());
      console.log(Number(tokenPayload.exp) * 1000 < new Date().getTime());
      revoked[token] = {
        token: token,
        exp: tokenPayload.exp,
      };
    }

    console.debug('REVOKED TOKENS:', JSON.stringify(revoked, null, '  '));

    // Generate new pair
    const result = await JWT.generatePair(tokenPayload);

    return result;
  };
}
