import { APIResponseJSON } from '../types';

export default function jsonRes<T = any>(data: APIResponseJSON<T>): string {
  return JSON.stringify(data, null, '  ');
}
