import { IncomingMessage, ServerResponse } from 'http';

export interface IAPIResponseFailure {
  status: 'failure';
  error: string;
}

export interface IAPIResponseSuccess<T = any> {
  status: 'success';
  data: T;
}

export type APIResponseJSON<T> = IAPIResponseFailure | IAPIResponseSuccess<T>;

export interface Post {
  title: string;
  body: string;
  userId: number;
  id: number;
}

export interface PostsStore {
  [key: number]: Post;
}

export interface User {
  id: number;
  email: string;
  password: string;
}

export interface UsersStore {
  [key: string]: User;
}

export interface IncomingMessageExtended extends IncomingMessage {
  params: {
    [key: string]: string;
  };
  pathname: string;
  searchParams: URLSearchParams;
  body?: {
    [key: string]: string;
  };
  [key: string]: any;
}

export type AppMiddleware = (
  request: IncomingMessageExtended,
  response: ServerResponse,
  next: () => void
) => void;

export interface RegularTokenPayload {
  type: 'regular';
  id: string;
  userId: string;
  [key: string]: any;
}

export interface RefreshTokenPayload {
  type: 'refresh';
  tokenId: string;
  [key: string]: any;
}

export type TokenPayload = RefreshTokenPayload | RegularTokenPayload;
