import { AppMiddleware, IncomingMessageExtended } from '../types';
import { RequestListener } from 'http';

// TODO:
// 1. Refactor: accept multiple middlewares in methods

/**
 * [pathTemplate, method, AppMiddleware]
 */
type StackEntry = [string, string, AppMiddleware];

/**
 * App class implements simple chain of responsibilities pattern
 */
export default class App {
  private _stack: StackEntry[] = [];

  private _addToStack = (
    pathTemplate: string,
    method: string,
    middleware: AppMiddleware
  ) => {
    this._stack.push([pathTemplate, method, middleware]);
  };

  private _parsePathParams = (pathname: string, template: string) => {
    const result: { [key: string]: string } = {};

    const pathChunks = pathname.substring(1).split('/');
    const templateChunks = template.substring(1).split('/');

    for (let i = 0; i < templateChunks.length; i++) {
      const templateChunk = templateChunks[i];

      if (templateChunk && templateChunk.startsWith(':')) {
        result[templateChunk.substring(1)] = pathChunks[i];
      }
    }

    return result;
  };

  use = (middleware: AppMiddleware) => {
    this._addToStack('*', '*', middleware);
  };

  all = (pathTemplate: string, middleware: AppMiddleware) => {
    this._addToStack(pathTemplate, '*', middleware);
  };

  get = (pathTemplate: string, middleware: AppMiddleware) => {
    this._addToStack(pathTemplate, 'GET', middleware);
  };

  post = (pathTemplate: string, middleware: AppMiddleware) => {
    this._addToStack(pathTemplate, 'POST', middleware);
  };

  listener: RequestListener = (req, res) => {
    // 1. Extend request object
    // 2. Filter middlewares by path and methods
    // 3. Call middlewares chain

    const { pathname, searchParams } = new URL(
      req.url || '',
      `http://${req.headers.host}`
    );

    const reqExtended: IncomingMessageExtended = Object.assign(req, {
      params: {},
      pathname,
      searchParams,
    });

    const currentStack = this._stack.filter(([pathTemplate, method]) => {
      let isMethodMatched = method === '*' ? true : method === req.method;

      let isPathMatched = false;

      if (pathTemplate === '*') {
        isPathMatched = true;
      } else if (pathTemplate.includes('/:')) {
        // contains param template
        // compare by parts
        const pathnameParts = pathname.split('/');
        const templateParts = pathTemplate.split('/');

        isPathMatched = templateParts.every((templatePart, i) => {
          switch (true) {
            case pathnameParts[i] === undefined:
              return false;

            case templatePart.startsWith(':'):
              return true;

            case templatePart === pathnameParts[i]:
              return true;

            default:
              return false;
          }
        });
      } else {
        isPathMatched = pathname === pathTemplate;
      }

      return isMethodMatched && isPathMatched;
    });

    const nextMiddleware = () => {
      const stackEntry = currentStack.shift();

      if (stackEntry) {
        const [pathTemplate, , middleware] = stackEntry;

        const params = this._parsePathParams(
          reqExtended.pathname,
          pathTemplate
        );
        reqExtended.params = params;

        middleware(reqExtended, res, nextMiddleware);
      }
    };

    nextMiddleware();
  };
}
