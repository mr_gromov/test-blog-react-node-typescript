import { AppMiddleware, TokenPayload } from '../types';
import { jsonRes, jwt } from '../utilities';

const protectPath: AppMiddleware = (req, res, next) => {
  const authHeader = req.headers['authorization'];

  if (!authHeader || !authHeader.startsWith('Bearer ')) {
    res.statusCode = 401;
    res.end(
      jsonRes({
        status: 'failure',
        error: 'Authorization token is absent',
      })
    );
    return;
  }

  const token = authHeader.substring(7);

  jwt
    .decode<TokenPayload>(token)
    .then((decoded) => {
      if (decoded && decoded.type === 'regular') {
        Object.assign(req, { jwtPayload: decoded });
        next();
      } else {
        res.statusCode = 401;
        res.end(
          jsonRes({
            status: 'failure',
            error: 'Invalid token',
          })
        );
      }
    })
    .catch((err) => {
      res.statusCode = 401;
      res.end(
        jsonRes({
          status: 'failure',
          error: `Token error: ${err.message}`,
        })
      );
    });
};

export default protectPath;
