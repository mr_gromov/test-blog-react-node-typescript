import fs from 'fs';
import path from 'path';
import { AppMiddleware } from '../types';

const mimeTypes: { [key: string]: string } = {
  '.html': 'text/html',
  '.js': 'text/javascript',
  '.css': 'text/css',
  '.json': 'application/json',
  '.png': 'image/png',
  '.jpg': 'image/jpg',
  '.gif': 'image/gif',
  '.wav': 'audio/wav',
  '.mp4': 'video/mp4',
  '.woff': 'application/font-woff',
  '.ttf': 'application/font-ttf',
  '.eot': 'application/vnd.ms-fontobject',
  '.otf': 'application/font-otf',
  '.svg': 'application/image/svg+xml',
};

type StaticFilesMiddleware = (staticRootDirectory: string) => AppMiddleware;

const staticFiles: StaticFilesMiddleware = (rootDir) => async (
  req,
  res,
  next
) => {
  const filePathParts = [rootDir];

  if (req.pathname.startsWith('/')) {
    filePathParts.push(...req.pathname.substring(1).split('/'));
  } else {
    filePathParts.push(...req.pathname.split('/'));
  }

  if (req.pathname.endsWith('/')) {
    filePathParts.push('index.html');
  }

  const filePath = path.join(...filePathParts);

  try {
    const stat = await fs.promises.stat(filePath);
    const extname = path.extname(filePath).toLowerCase();
    const contentType = mimeTypes[extname] || 'application/octet-stream';

    res.writeHead(200, {
      'Content-Type': contentType,
      'Content-Length': stat.size,
    });

    const fileStream = fs.createReadStream(filePath);

    fileStream
      .on('error', (error: NodeJS.ErrnoException | undefined) => {
        if (error) {
          throw error;
        }
      })
      .pipe(res);
  } catch (error) {
    if (error.code == 'ENOENT') {
      // not found
      next();
    } else {
      console.error(error);
      res.writeHead(500, { 'Content-Type': 'text/plain' });
      res.end(`INTERNAL SERVER ERROR`);
    }
  }
};

export default staticFiles;
