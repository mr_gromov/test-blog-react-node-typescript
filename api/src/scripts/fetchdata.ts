import fs from 'fs';
import path from 'path';
import fetch from 'node-fetch';
import { Post, PostsStore } from '../types';

const postsSourceUrl = 'https://jsonplaceholder.typicode.com/posts';
const postsFile = path.join(__dirname, '..', 'data', 'posts.json');
const usersFile = path.join(__dirname, '..', 'data', 'users.json');

const usersStore = {
  '1': {
    id: 1,
    email: 'john@example.com',
    password: 'john1234',
  },
};

(async () => {
  console.log(`Downloading "${postsSourceUrl}"`);
  const posts: Post[] = await fetch(postsSourceUrl).then((response) =>
    response.json()
  );

  // Check response
  const expectedProps: (keyof Post)[] = ['title', 'body', 'userId', 'id'];

  posts.forEach((post) => {
    expectedProps.forEach((prop) => {
      if (post && post[prop]) {
        return;
      }

      throw new Error(
        `Expected to receive collection of objects with props "${expectedProps}"\n` +
          `But received: "${JSON.stringify(post, null, '  ')}"`
      );
    });
  });

  // normalize
  const postsStore: PostsStore = posts.reduce((prev, curr) => {
    return { ...prev, [curr.id]: curr };
  }, {});

  await fs.promises.writeFile(
    postsFile,
    JSON.stringify(postsStore, null, '  ')
  );

  console.log(`Data saved to "${postsFile}"`);

  console.log(`Saved mocked users to "${usersFile}"`);
  await fs.promises.writeFile(
    usersFile,
    JSON.stringify(usersStore, null, '  ')
  );
})();
