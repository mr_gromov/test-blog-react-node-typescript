import fs from 'fs';
import path from 'path';
import { jsonRes, jwt } from '../utilities';
import { AppMiddleware, UsersStore, RegularTokenPayload } from '../types';

const users: UsersStore = JSON.parse(
  fs.readFileSync(path.join(__dirname, '..', 'data', 'users.json'), 'utf-8')
);

/**
 * Check provided tokens,
 * if there are valid "Refresh JWT" and correspondent JWT (userId is same)
 *  - Revoke provided "Refresh JWT" (TODO)
 *  - Revoke provided JWT if not expired (TODO)
 *  - Return new pair of JWT + "Refresh JWT"
 *
 */
const refresh: AppMiddleware = async (req, res) => {
  res.setHeader('Content-Type', 'application/json; charset=UTF-8');

  if (!req.body || !req.body.refreshToken || !req.body.token) {
    res.statusCode = 400;
    res.end(
      jsonRes({
        status: 'failure',
        error: 'Invalid payload, expecting JSON { token, refreshToken }',
      })
    );
    return;
  }

  try {
    const { token, refreshToken } = req.body;

    // check if user exists
    const tokenPayload = await jwt.decodeExpired<RegularTokenPayload>(token);

    if (!tokenPayload || !tokenPayload.userId) {
      throw new Error('Invalid token payload');
    }

    if (!users[tokenPayload.userId]) {
      throw new Error('User not authorized');
    }

    const tokensPair = await jwt.refresh({ token, refreshToken });

    res.end(jsonRes({ status: 'success', data: { ...tokensPair } }));
  } catch (error) {
    res.statusCode = 400;
    res.end(
      jsonRes({
        status: 'failure',
        error: `Invalid payload: ${error.message}`,
      })
    );

    return;
  }
};

export default refresh;
