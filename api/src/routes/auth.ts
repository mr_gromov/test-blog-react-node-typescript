import fs from 'fs';
import path from 'path';
import validator from 'validator';
import { jsonRes, jwt } from '../utilities';
import { AppMiddleware, UsersStore } from '../types';

const users: UsersStore = JSON.parse(
  fs.readFileSync(path.join(__dirname, '..', 'data', 'users.json'), 'utf-8')
);

console.debug('Users:');
console.debug(users);
console.debug('TODO: Hash passwords');

const auth: AppMiddleware = async (req, res) => {
  res.setHeader('Content-Type', 'application/json; charset=UTF-8');

  if (!req.body) {
    res.statusCode = 400;
    res.end(
      jsonRes({
        status: 'failure',
        error: 'Request body is absent',
      })
    );
    return;
  }

  const messages: string[] = [];

  if (!req.body.email || !validator.isEmail(req.body.email)) {
    messages.push('Please check "email" field');
  }

  if (!req.body.password || !validator.isAlphanumeric(req.body.password)) {
    messages.push('Please check "password" field');
  }

  if (messages.length) {
    res.statusCode = 400;
    res.end(
      jsonRes({
        status: 'failure',
        error: messages.join('\n'),
      })
    );
    return;
  }

  const { email, password } = req.body;

  const user = Object.values(users).find(
    (user) => email === user.email && password === user.password
  );

  if (!user) {
    res.statusCode = 401;
    res.end(
      jsonRes({
        status: 'failure',
        error: 'Unauthorized: user not found',
      })
    );

    return;
  }

  try {
    const tokensPair = await jwt.generatePair({ userId: user.id });

    res.end(jsonRes({ status: 'success', data: { ...tokensPair } }));
  } catch (error) {
    console.error(error);

    res.statusCode = 500;
    res.end(
      jsonRes({
        status: 'failure',
        error: 'INTERNAL SERVER ERROR',
      })
    );
  }
};

export default auth;
